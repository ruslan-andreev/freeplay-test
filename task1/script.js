/*Нужно написать набор функций для управления объектом в 2D пространстве:
Перемещение, поворот, перемещение с динамической скоростью(разбег-торможение).
Функции должны выдавать массивы значений, для промежутка времени. Например:
moveTo (from, to, duration, speed) и т.д.
*/

let  animationFigure = document.querySelector('#figure');

const move = (from, to, duration, speed) => {
    let startPosX = from[0];
    let startPosY = from[1];
    let id = setInterval(animation, 10);
    let start = Date.now();
    function animation() {
        let timePassed = Date.now() - start;
        if (timePassed >= duration) {
            clearInterval(id);
        } else { 
            if(startPosX<=to[0]) {
                if(Math.abs(from[0]-to[0]) >= Math.abs(from[1]-to[1])) {
                    startPosX += speed;
                    
                } 
                else startPosX += (Math.abs(from[0]-to[0]))/(Math.abs(from[1]-to[1]))*speed;
            } 
            if(startPosY<=to[1]) {
                if(Math.abs(from[0]-to[0]) >= Math.abs(from[1]-to[1])) {
                    startPosY += ((Math.abs(from[1]-to[1]))/(Math.abs(from[0]-to[0])))*speed;
                    
                }  
                else startPosY += speed;
            } 
            animationFigure.style.transform = 'matrix(' + 1 + ',' + 0 + ',' + 0 + ',' + 1 + ',' + startPosX + ',' + startPosY + ')';
        }
    }
}
//animationFigure.addEventListener('click', () => move([10,10], [200,200], 5000, 5))

const rotate = (position, duration, speed) => {
    let startPosX = position[0];
    let startPosY = position[1];
    let id = setInterval(animation, speed);
    let  angle = 0, currentAngle;
    let start = Date.now();
    function animation() {
        let timePassed = Date.now() - start;
        if (timePassed >= duration) {
            clearInterval(id);
        } else {
            currentAngle = angle++ * Math.PI / 180;
            animationFigure.style.transform = 'matrix(' + Math.cos(currentAngle) + ',' + Math.sin(currentAngle) + ',' + -Math.sin(currentAngle) + ',' + Math.cos(currentAngle) + ','  + startPosX + ',' + startPosY + ')';
        }
    }
}
//animationFigure.addEventListener('click', () => rotate([10,10], [200,200], 5000, 5))


const dynamicSpeed = (from, to, duration, speed) => {
    let startPosX = from[0];
    let startPosY = from[1];
    let id = setInterval(animation, speed);
    let counter = 1;
    let start = Date.now();
    function animation() {
        let timePassed = Date.now() - start;
        if (timePassed >= duration) {
            clearInterval(id);
        } else { 
            if(startPosX<=to[0]) {
                if(startPosX<Math.abs(from[0]-to[0])/2) counter = counter*1.2;
                else if (startPosX>Math.abs(from[0]-to[0])/2) counter = counter/1.2;
                if(Math.abs(from[0]-to[0]) >= Math.abs(from[1]-to[1]) && counter >= 0) {
                    startPosX += speed*counter;
                } 
                else startPosX += (Math.abs(from[0]-to[0]))/(Math.abs(from[1]-to[1]))*speed*counter;
            } 
            if(startPosY<=to[1]) {
                if(Math.abs(from[0]-to[0]) >= Math.abs(from[1]-to[1])) {
                    startPosY += ((Math.abs(from[1]-to[1]))/(Math.abs(from[0]-to[0])))*speed*counter;
                    
                }  
                else startPosY += speed*counter;
            } 
            animationFigure.style.transform = 'matrix(' + 1 + ',' + 0 + ',' + 0 + ',' + 1 + ',' + startPosX + ',' + startPosY + ')';
        }
    }
}
//animationFigure.addEventListener('click', () => dynamicSpeed([10,10], [600,10], 1000, 5))
